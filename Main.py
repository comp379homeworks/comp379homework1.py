# Titanic
# By Allan Miller

import numpy as np
import pandas as pd


# Show all table Columns
pd.set_option('display.max_columns', None)
# Show all table Rows
pd.set_option('display.max_rows', 10)
# Show as many columns per line as possible
pd.set_option('display.max_colwidth', -1)
# Import training and testing data from file
data_train = pd.read_csv('Data/train copy.csv')  # Success!!
data_test  = pd.read_csv('Data/test copy.csv') # Success!!
survived_keys = pd.read_csv('Data/gender_submission copy.csv')



def preprocess(dataframe):

    dataframe = dataframe.drop(columns=['Age','Cabin','Name','Ticket'])
    sex_mapping = {label: idx for idx, label in enumerate(np.unique(dataframe['Sex']))}
    dataframe['Sex'] = dataframe['Sex'].map(sex_mapping)
    dataframe = dataframe.rename(columns={'Sex': 'Male'})
    dataframe = pd.get_dummies(dataframe, drop_first=True)
    dataframe = dataframe.rename(columns={'Embarked_Q': 'Q', 'Embarked_S': 'S'})

    dataframe = dataframe.fillna(dataframe.mean())


    processed_dataframe = dataframe
    return processed_dataframe

def normalize(dataframe):
    dataframe = dataframe.set_index('PassengerId')
    normalized_dataframe = (dataframe - dataframe.min()) / (dataframe.max() - dataframe.min())
    return normalized_dataframe

def compute_accuracy(model, X_test, y_test):
    prediction = model.predict(X_test)
    prediction_df = pd.DataFrame(data={'predictions': prediction})
    score_prediction = pd.DataFrame.join(y_test, prediction_df)
    score_prediction['Match'] = score_prediction.apply(lambda row: row.Survived == row.predictions, axis=1)
    accuracy = score_prediction['Match'].sum() / score_prediction.shape[0]
    return accuracy

norm_train = normalize(preprocess(data_train))

norm_test  = normalize(preprocess(data_test))

X_train = norm_train.drop(columns='Survived')
y_train = norm_train[['Survived']]
X_test  = norm_test
y_test  = survived_keys

#LOGISTIC REGRESSION
from sklearn.linear_model import LogisticRegression
lr = LogisticRegression(C = 100.0, random_state=1)
lr.fit(X_train, y_train.values.ravel())
lr_accuracy = compute_accuracy(lr,X_test,y_test)
#RANDOM FOREST
from sklearn.ensemble import RandomForestClassifier
forest = RandomForestClassifier(criterion='gini',n_estimators=25,random_state=1,n_jobs=2)
forest.fit(X_train, y_train.values.ravel())
forest_accuracy = compute_accuracy(forest,X_test,y_test)
#KNN
from sklearn.neighbors import KNeighborsClassifier
knn = KNeighborsClassifier(n_neighbors=5,p=2, metric='minkowski')
knn.fit(X_train, y_train.values.ravel())
knn.accuracy = compute_accuracy(knn,X_test,y_test)

print("Logistic regression accuracy is: " + repr(lr_accuracy*100) + '%')
print("Random Forest accuracy is: " + repr(forest_accuracy*100) + '%')
print("KNN accuracy is: " + repr(knn.accuracy*100) + '%')















